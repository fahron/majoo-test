import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Widget textButton(
    {required String message,
    required String textbutton,
    required Function onPress}) {
  return Align(
    alignment: Alignment.center,
    child: RichText(
      text: TextSpan(
          text: message,
          style: GoogleFonts.poppins(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Color.fromARGB(255, 119, 119, 119),
          ),
          children: [
            TextSpan(
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  onPress();
                  // Navigator.pop(context);
                },
              text: textbutton,
              style: GoogleFonts.poppins(
                fontSize: 14,
                fontWeight: FontWeight.w600,
                color: Color(0xff5568FE),
              ),
            ),
          ]),
    ),
  );
}

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Widget logoAuth({required String title, required String subtitle}) {
  return Column(
    children: [
      Icon(
        Icons.movie,
        size: 90,
        color: Color(0xff5568FE),
      ),
      Center(
        child: Text(
          title,
          style: GoogleFonts.poppins(fontSize: 25, fontWeight: FontWeight.w600),
        ),
      ),
      Center(
        child: Text(
          subtitle,
          style: GoogleFonts.poppins(
            fontSize: 12,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
      SizedBox(
        height: 50,
      ),
    ],
  );
}

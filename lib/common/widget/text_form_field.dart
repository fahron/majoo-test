import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'form_field.dart';

class CustomTextFormField extends CustomFormField<String> {
  CustomTextFormField({
    String label = "",
    required TextInputType keyboardtype,
    String? title,
    String hint = "",
    required TextController controller,
    BuildContext? context,
    bool enabled = true,
    bool mandatory = true,
    bool isObscureText = false,
    bool isEmail = false,
    bool isDigit = false,
    bool search = true,
    FormFieldValidator<String>? validator,
    double padding = 4,
    TextInputAction? textInputAction,
    String pesanValidation = "",
    Widget? suffixIcon,
    Key? key,
  }) : super(
          key: key,
          controller: controller,
          enabled: enabled,
          builder: (FormFieldState<String> state) {
            return _CustomTextForm(
              keyboardtype: keyboardtype,
              pesanValidation: pesanValidation,
              label: label,
              controller: controller,
              hint: hint,
              state: state,
              mandatory: mandatory,
              isObscureText: isObscureText,
              suffixIcon: suffixIcon!,
              isEmail: isEmail,
              isDigit: isDigit,
              padding: padding,
              search: search,
              textInputAction: textInputAction!,
            );
          },
          validator: (picker) {
            final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
            if (mandatory && (picker == null || picker.isEmpty)) {
              return pesanValidation;
            } else if (isEmail == true && pattern.hasMatch(picker!) == false) {
              return "Email tidak valid";
            }

            //return "user tidak ditemukan";
          },
        );
}

class TextController extends CustomFormFieldController<String> {
  TextController({String? initialValue}) : super(initialValue = "");

  @override
  String fromValue(String value) => value;

  @override
  String toValue(String text) => text;
}

class _CustomTextForm extends StatefulWidget {
  final FormFieldState<String> state;
  final TextController controller;
  final double padding;
  final String label;
  final String hint;
  final bool mandatory;
  final bool isObscureText;
  final bool isEmail;
  final bool isDigit;
  final TextInputAction textInputAction;
  final Widget suffixIcon;
  final TextInputType keyboardtype;
  final bool search;

  const _CustomTextForm({
    required this.state,
    required this.controller,
    required this.label,
    required this.hint,
    this.padding = 0,
    this.isObscureText = false,
    this.isEmail = false,
    this.mandatory = false,
    this.isDigit = false,
    required this.textInputAction,
    required this.suffixIcon,
    required String pesanValidation,
    required this.keyboardtype,
    required bool this.search,
  });

  @override
  State<StatefulWidget> createState() => _CustomTextFormState();
}

class _CustomTextFormState extends State<_CustomTextForm> {
  final _focusNode = FocusNode();

  String get _hint => widget.hint;
  TextInputType get _keyboardtype => widget.keyboardtype;

  bool get _mandatory => widget.mandatory;

  String get _label {
    var fullLabel = StringBuffer();
    final label = widget.label;
    fullLabel.write(label);
    if (_mandatory) fullLabel.write(label.isEmpty ? '' : ' *');
    return fullLabel.toString();
  }

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    final inputFormatters = <TextInputFormatter>[];
    final keyboardtype = this._keyboardtype;

    final _hint = this._hint;
    return Padding(
      padding: EdgeInsets.only(bottom: widget.padding, top: widget.padding),
      child: ConstrainedBox(
        constraints: const BoxConstraints(
          maxHeight: 300,
        ),
        child: LayoutBuilder(
          builder: (context, constraints) {
            return IntrinsicHeight(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _label != null
                      ? Text(_label,
                          style: GoogleFonts.poppins(
                              fontSize: 12, fontWeight: FontWeight.w500))
                      : SizedBox.shrink(),
                  SizedBox(height: 3),
                  Stack(
                    children: <Widget>[
                      Container(
                        child: TextField(
                          style: GoogleFonts.poppins(
                              fontSize: 14, fontWeight: FontWeight.w400),
                          textInputAction: widget.textInputAction,
                          keyboardType: keyboardtype,
                          inputFormatters: inputFormatters,
                          focusNode: _focusNode,
                          textAlignVertical:
                              _label == null ? TextAlignVertical.center : null,
                          controller: widget.controller.textController,
                          decoration: defaultInputDecoration.copyWith(
                              contentPadding: const EdgeInsets.symmetric(
                                  vertical: 0.0, horizontal: 10),
                              fillColor: Colors.grey.withOpacity(0.2),
                              filled: true,
                              errorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.red),
                                  borderRadius: BorderRadius.circular(5.0)),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius: BorderRadius.circular(5.0)),
                              hintStyle: GoogleFonts.poppins(
                                  fontSize: 14, fontWeight: FontWeight.w300),
                              hintText: _hint,
                              errorStyle: GoogleFonts.poppins(
                                  fontSize: 12, fontWeight: FontWeight.w300),
                              disabledBorder: InputBorder.none,
                              isDense: _label == null,
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.always,
                              errorText: widget.state.errorText,
                              alignLabelWithHint: true,
                              suffixIcon: widget.search == true
                                  ? widget.suffixIcon
                                  : (_focusNode.hasFocus
                                      ? widget.suffixIcon
                                      : null)),
                          obscureText: widget.isObscureText,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

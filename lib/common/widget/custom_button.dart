import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final double height;
  final VoidCallback onPressed;
  final Widget leadingIcon;
  final bool isSecondary;
  final bool isOutline;

  const CustomButton(
      {Key? key,
      this.text = "",
      this.height = 50,
      required this.onPressed,
      required this.leadingIcon,
      this.isSecondary = false})
      : isOutline = false,
        super(key: key);

  const CustomButton.outline(
      {Key? key,
      required this.text,
      this.height = 50,
      required this.onPressed,
      required this.leadingIcon,
      this.isSecondary = false})
      : isOutline = true,
        super(key: key);

  OutlinedBorder _shapeBorder(BuildContext context) => RoundedRectangleBorder(
        side: isSecondary
            ? BorderSide(
                color: Theme.of(context).primaryColor,
                width: 1,
                style: BorderStyle.solid)
            : BorderSide.none,
        borderRadius: BorderRadius.circular(8),
      );

  Widget _label(BuildContext context) {
    return Expanded(
      child: Text(
        text,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.center,
        style: TextStyle(color: _color(context)),
      ),
    );
  }

  Color _color(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final color = isSecondary ? Colors.transparent : primaryColor;
    return isSecondary
        ? primaryColor
        : color.computeLuminance() > 0.5
            ? Colors.black
            : Colors.white;
  }

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final color = isSecondary ? Colors.transparent : primaryColor;
    return ButtonTheme(
        height: height,
        buttonColor: color,
        minWidth: double.infinity,
        child: Container(
          width: double.infinity,
          child: InkWell(
            onTap: onPressed,
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Color(0xff5568FE),
              ),
              child: Center(
                child: Text(
                  text,
                  style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Colors.white),
                ),
              ),
            ),
          ),
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void showSnackBar(String message, Color color, BuildContext context) {
  final snackBar = SnackBar(
    content: Text(
      message,
      style: GoogleFonts.poppins(color: Colors.white),
    ),
    backgroundColor: color,
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

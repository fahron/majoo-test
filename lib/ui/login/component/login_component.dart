import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import '../../../bloc/auth_bloc/auth_bloc_cubit.dart';
import '../../../sqflite/database_sqflite.dart';
import 'package:majootestcase/common/widget/snack_bar.dart';

Widget formLogin({
  required Key formKey,
  required TextController emailController,
  required BuildContext context,
  required TextController passwordController,
  required bool isObscurePassword,
  required Widget suffixIcon,
}) {
  return Form(
    key: formKey,
    child: Column(
      children: [
        CustomTextFormField(
          search: false,
          keyboardtype: TextInputType.emailAddress,
          pesanValidation: "Email tidak boleh kosong",
          textInputAction: TextInputAction.done,
          suffixIcon: InkWell(
              onTap: () {
                emailController.textController.text = "";
              },
              child: Icon(Icons.highlight_off)),
          context: context,
          controller: emailController,
          isEmail: true,
          hint: 'example@mail.com',
          label: 'Email',
          validator: (val) {
            final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
            if (val != null)
              return pattern.hasMatch(val) ? null : 'email is invalid';
          },
        ),
        CustomTextFormField(
            search: false,
            keyboardtype: TextInputType.name,
            textInputAction: TextInputAction.done,
            pesanValidation: "Password tidak boleh kosong",
            context: context,
            label: 'Password',
            hint: 'password',
            controller: passwordController,
            isObscureText: isObscurePassword,
            suffixIcon: suffixIcon),
      ],
    ),
  );
}

void handleLogin(
    {required TextController emailController,
    required TextController passwordController,
    required GlobalKey<FormState> formKey,
    required SqfliteDb db,
    required BuildContext context}) async {
  final _email = emailController.value;
  final _password = passwordController.value;
  if (formKey.currentState!.validate() &&
      _email.isNotEmpty &&
      _password.isNotEmpty) {
    final allRows = await db.queryAllRows();

    if (allRows
        .where((e) => (e["email"] == _email && e["password"] == _password))
        .isNotEmpty) {
      (context).read<AuthBlocCubit>().emit(AuthBlocLoggedInState());
      showSnackBar("Login Berhasil", Colors.green, context);
      AuthBlocCubit().login_user();
    } else {
      showSnackBar(
          "Login gagal, Email dan password salah", Colors.red, context);
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/component/login_component.dart';
import 'package:majootestcase/ui/register/register_page.dart';
import '../../common/widget/logo.dart';
import '../../sqflite/database_sqflite.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  final db = SqfliteDb.instance;
  final AuthBlocCubit _authBlocCubit = AuthBlocCubit();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark));
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        bloc: _authBlocCubit,
        listener: (context, state) {
          if (state is AuthBlocLoggedInState) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => HomeBlocCubit()..fetching_data(),
                  child: HomeBlocScreen(),
                ),
              ),
            );
          } else if (state is AuthBlocErrorState) {
            print(state.error);
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 100, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                logoAuth(
                    subtitle: "Silahkan login terlebih dahulu",
                    title: "Selamat Datang!"),
                formLogin(
                  context: context,
                  emailController: _emailController,
                  formKey: formKey,
                  isObscurePassword: _isObscurePassword,
                  passwordController: _passwordController,
                  suffixIcon: IconButton(
                    icon: Icon(
                      _isObscurePassword
                          ? Icons.visibility_off_outlined
                          : Icons.visibility_outlined,
                    ),
                    onPressed: () {
                      setState(() {
                        _isObscurePassword = !_isObscurePassword;
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                CustomButton(
                  leadingIcon: Icon(Icons.error),
                  text: 'Login',
                  onPressed: () {
                    handleLogin(
                        context: context,
                        db: db,
                        emailController: _emailController,
                        formKey: formKey,
                        passwordController: _passwordController);
                    // print(_authBlocCubit.state.toString());
                  },
                  height: 50,
                ),
                SizedBox(
                  height: 20,
                ),
                textButton(
                    message: "Belum punya akun?",
                    textbutton: " Daftar",
                    onPress: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: ((context) => RegisterPage())));
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

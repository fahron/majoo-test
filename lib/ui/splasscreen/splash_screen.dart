import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../bloc/auth_bloc/auth_bloc_cubit.dart';
import '../../main.dart';

class SplassScreen extends StatefulWidget {
  const SplassScreen({Key? key}) : super(key: key);

  @override
  State<SplassScreen> createState() => _SplassScreenState();
}

class _SplassScreenState extends State<SplassScreen> {
  bool loading = true;
  late Timer _timer;
  @override
  void initState() {
    _timer = Timer(Duration(seconds: 2), () {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (_) => BlocProvider(
              create: (context) => AuthBlocCubit()..fetch_history_login(),
              child: MyHomePageScreen(),
            ),
          ),
          (route) => false);
    });
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark));
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Icon(
          Icons.movie,
          size: 110,
          color: Color(0xff5568FE),
        ),
      ),
    );
  }
}

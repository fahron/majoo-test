import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import '../home_bloc/home_bloc_screen.dart';

class ErrorConection extends StatefulWidget {
  const ErrorConection({Key? key}) : super(key: key);

  @override
  State<ErrorConection> createState() => _ErrorConectionState();
}

class _ErrorConectionState extends State<ErrorConection> {
  bool loading = true;
  late Timer _timer;
  @override
  void initState() {
    _timer = Timer(Duration(seconds: 2), () {
      setState(() {
        loading = false;
      });
    });
    // TODO: implement initState

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: loading == true
            ? CircularProgressIndicator()
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.wifi_off_rounded,
                    size: 80,
                    color: Color(0xff5568FE),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Tidak ada koneksi internet",
                    style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff5568FE),
                    ),
                  ),
                  SizedBox(height: 10),
                  InkWell(
                    onTap: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (_) => BlocProvider(
                              create: (context) =>
                                  HomeBlocCubit()..fetching_data(),
                              child: HomeBlocScreen(),
                            ),
                          ),
                          (route) => false);
                    },
                    child: Container(
                      height: 40,
                      width: 90,
                      decoration: BoxDecoration(
                          color: Color(0xff5568FE),
                          borderRadius: BorderRadius.circular(5)),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "Muat ulang",
                            style: GoogleFonts.poppins(
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
      ),
    );
  }
}

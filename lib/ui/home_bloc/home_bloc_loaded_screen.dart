import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/detail/detail_page.dart';
import 'package:majootestcase/ui/home_bloc/component/home_component.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import '../../common/widget/text_form_field.dart';

// ignore: must_be_immutable
class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results> data;

  HomeBlocLoadedScreen({Key? key, required this.data}) : super(key: key);
  Future<void> _onRefres() async {}
  final AuthBlocCubit _authBlocCubit = AuthBlocCubit();

  TextController _searchCntrl = TextController();
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _onRefres,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: BlocListener<AuthBlocCubit, AuthBlocState>(
          bloc: _authBlocCubit,
          listener: (context, state) {
            if (state is AuthBlocLoginState) {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                  (route) => false);
            }
          },
          child: SafeArea(
            child: ListView.builder(
              itemCount: data == null ? 0 : data.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding:
                      EdgeInsets.only(top: 20, right: 18, left: 18, bottom: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      index == 0
                          ? headerComponent(
                              context: context, searchCntrl: _searchCntrl)
                          : Container(),
                      InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => DetailPage(
                                        date: data[index].releaseDate ??
                                            data[index].firstAirDate!,
                                        imageUrl: data[index].posterPath!,
                                        overview: data[index].overview!,
                                        popularity:
                                            data[index].popularity!.toString(),
                                        rating:
                                            data[index].voteAverage!.toString(),
                                        title: data[index].originalTitle ??
                                            data[index].originalName!,
                                      )),
                            );
                          },
                          child: cardMovie(
                            image: data[index].posterPath!,
                            overView: data[index].overview!,
                            title: data[index].originalTitle ??
                                data[index].originalName!,
                            voteAvarage: data[index].voteAverage.toString(),
                          )),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import '../../../bloc/auth_bloc/auth_bloc_cubit.dart';

Widget cardMovie(
    {required String image,
    required String title,
    required String overView,
    required String voteAvarage}) {
  return Row(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Container(
        width: 100,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Image.network(
            "https://image.tmdb.org/t/p/w500/" + image,
            fit: BoxFit.cover,
          ),
        ),
      ),
      SizedBox(
        width: 20,
      ),
      Expanded(
          child: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: GoogleFonts.poppins(
                  fontSize: 18, fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              overView,
              overflow: TextOverflow.ellipsis,
              maxLines: 3,
              style: GoogleFonts.poppins(
                  fontSize: 12, fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Icon(
                  Icons.star,
                  color: Colors.yellow,
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  voteAvarage,
                  style: GoogleFonts.poppins(
                      fontSize: 16, fontWeight: FontWeight.w400),
                )
              ],
            ),
          ],
        ),
      ))
    ],
  );
}

Widget headerComponent(
    {required BuildContext context, required TextController searchCntrl}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Icon(
            Icons.account_circle_sharp,
            size: 40,
            color: Color(0xff5568FE),
          ),
          InkWell(
            onTap: () {
              (context).read<AuthBlocCubit>().emit(AuthBlocLoginState());
              AuthBlocCubit().logout_user();
            },
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Color(0xff5568FE)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.logout,
                  size: 20,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
      CustomTextFormField(
        keyboardtype: TextInputType.name,
        textInputAction: TextInputAction.search,
        pesanValidation: "",
        context: context,
        hint: 'Search movie',
        controller: searchCntrl,
        suffixIcon: Icon(Icons.search),
      ),
      Padding(
        padding: const EdgeInsets.only(top: 20, bottom: 20),
        child: Text("New Movie",
            style:
                GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600)),
      )
    ],
  );
}

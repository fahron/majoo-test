import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/ui/extra/error_connection.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_bloc_loaded_screen.dart';
import '../extra/error_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  const HomeBlocScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
      if (state is HomeBlocLoadedState) {
        return HomeBlocLoadedScreen(data: state.data);
      } else if (state is HomeBlocLoadingState) {
        return Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      } else if (state is HomeBlocInitialState) {
        return ErrorConection();
      } else if (state is HomeBlocErrorState) {
        return ErrorScreen(
            retry: () {},
            retryButton: Container(),
            textColor: Colors.amber,
            message: state.error);
      }

      return Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""));
    });
  }
}

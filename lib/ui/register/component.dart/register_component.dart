import 'package:flutter/material.dart';

import '../../../common/widget/text_form_field.dart';

Widget formRegister(
    {required Key fromKey,
    required TextController usernameController,
    required BuildContext context,
    required TextController emailController,
    required TextController passwordController,
    required bool isObscurePassword,
    required Widget suffixIcon}) {
  return Form(
    key: fromKey,
    child: Column(
      children: [
        CustomTextFormField(
          search: false,
          keyboardtype: TextInputType.name,
          pesanValidation: "Username tidak boleh kosong",
          textInputAction: TextInputAction.done,
          suffixIcon: InkWell(
              onTap: () {
                usernameController.textController.text = "";
              },
              child: Icon(Icons.highlight_off)),
          context: context,
          controller: usernameController,
          isEmail: false,
          hint: 'username',
          label: 'Username',
          validator: (val) {
            final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
            if (val != null)
              return pattern.hasMatch(val) ? null : 'email is invalid';
          },
        ),
        CustomTextFormField(
          search: false,
          keyboardtype: TextInputType.emailAddress,
          pesanValidation: "Email tidak boleh kosong",
          textInputAction: TextInputAction.done,
          suffixIcon: InkWell(
              onTap: () {
                emailController.textController.text = "";
              },
              child: Icon(Icons.highlight_off)),
          context: context,
          controller: emailController,
          isEmail: true,
          hint: 'example@mail.com',
          label: 'Email',
          validator: (val) {
            final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
            if (val != null)
              return pattern.hasMatch(val) ? null : 'email is invalid';
          },
        ),
        CustomTextFormField(
            search: false,
            keyboardtype: TextInputType.name,
            textInputAction: TextInputAction.done,
            pesanValidation: "Password tidak boleh kosong",
            context: context,
            label: 'Password',
            hint: 'password',
            controller: passwordController,
            isObscureText: isObscurePassword,
            suffixIcon: suffixIcon),
      ],
    ),
  );
}

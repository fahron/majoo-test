import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/snack_bar.dart';
import 'package:majootestcase/common/widget/text_button.dart';
import 'package:majootestcase/sqflite/database_sqflite.dart';
import 'package:majootestcase/ui/register/component.dart/register_component.dart';
import '../../bloc/auth_bloc/auth_bloc_cubit.dart';
import '../../common/widget/logo.dart';
import '../../common/widget/text_form_field.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _usernameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  final AuthBlocCubit _authBlocCubit = AuthBlocCubit();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final db = SqfliteDb.instance;
  bool _isObscurePassword = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
          bloc: _authBlocCubit,
          listener: (context, state) {
            if (state is AuthBlocLoggedInState) {
              Navigator.pop(context);
            } else if (state is AuthBlocErrorState) {
              print(state.error);
            }
          },
          child: Padding(
            padding: const EdgeInsets.only(right: 18, left: 18, top: 50),
            child: ListView(
              children: [
                logoAuth(
                    subtitle: "Silahkan daftar terlebih dahulu",
                    title: "Selamat Datang!"),
                formRegister(
                    context: context,
                    emailController: _emailController,
                    fromKey: formKey,
                    isObscurePassword: _isObscurePassword,
                    suffixIcon: IconButton(
                        icon: Icon(
                          _isObscurePassword
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                        ),
                        onPressed: () {
                          setState(() {
                            _isObscurePassword = !_isObscurePassword;
                          });
                        }),
                    passwordController: _passwordController,
                    usernameController: _usernameController),
                SizedBox(
                  height: 40,
                ),
                CustomButton(
                  leadingIcon: Icon(Icons.error),
                  text: 'Register',
                  onPressed: () {
                    final _email = _emailController.value;
                    final _password = _passwordController.value;
                    final _userName = _usernameController.value;
                    if (formKey.currentState!.validate() &&
                        _email.isNotEmpty &&
                        _password.isNotEmpty &&
                        _userName.isNotEmpty) {
                      try {
                        db.insert(<String, dynamic>{
                          SqfliteDb.columnName: _usernameController.value,
                          SqfliteDb.columnEmail: _emailController.value,
                          SqfliteDb.columnPassword: _passwordController.value
                        });
                        showSnackBar("Register berhasil, silahkan login",
                            Colors.green, context);
                        Navigator.pop(context);
                      } catch (error) {
                        showSnackBar("Register gagal, terjadi kesalahan",
                            Colors.red, context);
                      }
                    }
                  },
                  height: 50,
                ),
                SizedBox(
                  height: 20,
                ),
                textButton(
                    message: "Sudah punya akun?",
                    textbutton: " Masuk",
                    onPress: () {
                      Navigator.pop(context);
                    }),
              ],
            ),
          )),
    );
  }
}

class MovieModel {
  int? page;
  List<Results>? results;
  int? totalPages;
  int? totalResults;

  MovieModel({this.page, this.results, this.totalPages, this.totalResults});

  MovieModel.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    if (json['results'] != null) {
      results = <Results>[];
      json['results'].forEach((v) {
        results!.add(new Results.fromJson(v));
      });
    }
    totalPages = json['total_pages'];
    totalResults = json['total_results'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    if (this.results != null) {
      data['results'] = this.results!.map((v) => v.toJson()).toList();
    }
    data['total_pages'] = this.totalPages;
    data['total_results'] = this.totalResults;
    return data;
  }
}

class Results {
  String? backdropPath;
  List<int>? genreIds;
  String? originalLanguage;
  String? originalTitle;
  String? posterPath;
  bool? video;
  int? id;
  int? voteCount;
  String? overview;
  String? releaseDate;
  String? title;
  double? voteAverage;
  bool? adult;
  double? popularity;
  String? mediaType;
  String? originalName;
  List<String>? originCountry;
  String? firstAirDate;
  String? name;

  Results(
      {this.backdropPath,
      this.genreIds,
      this.originalLanguage,
      this.originalTitle,
      this.posterPath,
      this.video,
      this.id,
      this.voteCount,
      this.overview,
      this.releaseDate,
      this.title,
      this.voteAverage,
      this.adult,
      this.popularity,
      this.mediaType,
      this.originalName,
      this.originCountry,
      this.firstAirDate,
      this.name});

  Results.fromJson(Map<String, dynamic> json) {
    backdropPath = json['backdrop_path'];

    originalLanguage = json['original_language'];
    originalTitle = json['original_title'];
    posterPath = json['poster_path'];
    video = json['video'];
    id = json['id'];
    voteCount = json['vote_count'];
    overview = json['overview'];
    releaseDate = json['release_date'];
    title = json['title'];
    voteAverage = json['vote_average'];
    adult = json['adult'];
    popularity = json['popularity'];
    mediaType = json['media_type'];
    originalName = json['original_name'];

    firstAirDate = json['first_air_date'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['backdrop_path'] = this.backdropPath;
    data['genre_ids'] = this.genreIds;
    data['original_language'] = this.originalLanguage;
    data['original_title'] = this.originalTitle;
    data['poster_path'] = this.posterPath;
    data['video'] = this.video;
    data['id'] = this.id;
    data['vote_count'] = this.voteCount;
    data['overview'] = this.overview;
    data['release_date'] = this.releaseDate;
    data['title'] = this.title;
    data['vote_average'] = this.voteAverage;
    data['adult'] = this.adult;
    data['popularity'] = this.popularity;
    data['media_type'] = this.mediaType;
    data['original_name'] = this.originalName;
    data['origin_country'] = this.originCountry;
    data['first_air_date'] = this.firstAirDate;
    data['name'] = this.name;
    return data;
  }
}

import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<MovieModel> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      var data = await dio.get(
          "https://api.themoviedb.org/3/trending/all/day?api_key=c95b7d97f00d7258715296f44a0c37f0");

      return MovieModel.fromJson(data.data);
    } catch (e) {
      print(e.toString());
      return MovieModel();
    }
  }
}
